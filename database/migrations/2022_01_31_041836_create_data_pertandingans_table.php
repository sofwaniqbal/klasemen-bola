<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPertandingansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pertandingans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('klub1id');
            $table->string('home');
            $table->unsignedBigInteger('klub2id');
            $table->string('away');
            $table->bigInteger('skor1');
            $table->bigInteger('skor2');
            $table->bigInteger('stat');
            $table->timestamps();

            $table->foreign('klub1id')->references('id')->on('data_clubs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('klub2id')->references('id')->on('data_clubs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pertandingans');
    }
}