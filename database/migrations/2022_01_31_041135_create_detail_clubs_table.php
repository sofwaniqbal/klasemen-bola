<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_clubs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('club_id');
            $table->bigInteger('menang');
            $table->bigInteger('seri');
            $table->bigInteger('kalah');
            $table->bigInteger('gol_masuk');
            $table->bigInteger('gol_kemasukan');
            $table->bigInteger('gol_selisih');
            $table->bigInteger('stat');
            $table->timestamps();

            $table->foreign('club_id')->references('id')->on('data_clubs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_clubs');
    }
}