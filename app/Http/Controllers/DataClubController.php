<?php

namespace App\Http\Controllers;

use App\Models\DataClub;
use App\Models\DetailClub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class DataClubController extends Controller
{
    public function index()
    {
        # code...
    }

    public function auth()
    {
        $message = ['message' => 'Anda tidak memiliki akses'];
        return redirect()->route('tambahklub')->with(['error' => $message['message']]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'namaklub' => 'required|unique:data_clubs,nama_klub',
                'kotaklub' => 'required',
            ],
            [
                'namaklub.required' => 'Nama Klub wajib diisi.',
                'namaklub.unique' => 'Nama Klub sudah terisi.',
                'kotaklub.required' => 'Kota Klub wajib diisi.',
            ]
        );

        if ($validator->fails()) {
            return redirect()->route('tambahklub')->withErrors($validator);
        } else {
            try {
                $register = DataClub::create([
                    'nama_klub' => $request->namaklub,
                    'kota' => str::ucfirst($request->kotaklub),
                ]);

                if ($register) {
                    DetailClub::create([
                        'club_id' => $register->id,
                        'menang' => 0,
                        'seri' => 0,
                        'kalah' => 0,
                        'gol_masuk' => 0,
                        'gol_kemasukan' => 0,
                        'gol_selisih' => 0,
                        'point' => 0,
                        'stat' => 0
                    ]);

                    $message = ['message' => 'Menambah klub berhasil.'];
                    return redirect()->route('tambahklub')->with(['berhasil' => $message['message']]);
                } else {
                    $message = ['message' => 'Gagal menambah klub.'];
                    return redirect()->route('tambahklub')->with(['error' => $message['message']]);
                }
            } catch (\Exception $e) {
                return redirect()->route('tambahklub')->with(['error' => $e->getMessage()]);
            }
        }
    }
}