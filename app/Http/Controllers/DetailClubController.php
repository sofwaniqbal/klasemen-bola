<?php

namespace App\Http\Controllers;

use App\Models\DataClub;
use App\Models\DataPertandingan;
use App\Models\DetailClub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DetailClubController extends Controller
{
    public function index()
    {

        // $data = DetailClub::select('nama_klub', 'menang', 'seri', 'kalah', 'gol_masuk', 'gol_kemasukan')
        //     ->join('data_clubs', 'data_clubs.id', '=', 'club_id')->get();
        $data = DetailClub::select('club_id', 'nama_klub',DB::raw('sum(menang)+sum(seri)+sum(kalah) as main'), 'menang', 'seri', 'kalah', 'gol_masuk', 'gol_kemasukan', DB::raw('sum(gol_masuk)-sum(gol_kemasukan) as selisih'), DB::raw('sum(menang)*3+sum(seri)*1+sum(kalah)*0 as poin'))
            ->groupBy('club_id')
            ->orderBy('poin', 'desc')
            ->orderBy('selisih', 'desc')
            ->orderBy('gol_kemasukan', 'asc')
            ->join('data_clubs', 'data_clubs.id', '=', 'club_id')->get();
        // return $data;

        return view('data-klasemen', ['dataclub' => $data]);
    }
}