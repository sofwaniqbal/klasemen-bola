<?php

namespace App\Http\Controllers;

use App\Models\DataClub;
use App\Models\DataPertandingan;
use App\Models\DetailClub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class DataPertandinganController extends Controller
{
    public function index()
    {
        return view('pertandingan');
    }

    public function auth()
    {
        $message = ['message' => 'Anda tidak memiliki akses'];
        return redirect()->route('listmatch')->with(['error' => $message['message']]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'homeklub' => 'required',
                'awayklub' => 'required',
            ],
            [
                'homeklub.required' => 'Home Klub wajib diisi.',
                'awayklub.required' => 'Away Klub wajib diisi.',
            ]
        );

        if ($validator->fails()) {
            return redirect()->route('tambahmatch')->withErrors($validator);
        } else {
            try {
                $check = DataPertandingan::where([['klub1id', $request->homeklub], ['klub2id', $request->awayklub]])->first();
                if ($check) {
                    $message = ['message' => 'Data Pertandingan sudah ada.'];
                    return redirect()->route('tambahmatch')->with(['error' => $message['message']]);
                } elseif ($request->homeklub === $request->awayklub) {
                    $message = ['message' => 'Data Pertandingan tidak memungkinkan.'];
                    return redirect()->route('tambahmatch')->with(['error' => $message['message']]);
                } else {
                    $home = DataClub::where('id', $request->homeklub)->first();
                    $away = DataClub::where('id', $request->awayklub)->first();
                    $register = DataPertandingan::create([
                        'klub1id' => $request->homeklub,
                        'home' => $home->nama_klub,
                        'klub2id' => $request->awayklub,
                        'away' => $away->nama_klub,
                        'skor1' => 0,
                        'skor2' => 0,
                        'stat' => 0,
                    ]);

                    if ($register) {

                        $message = ['message' => 'Menambah pertandingan berhasil.'];
                        return redirect()->route('tambahmatch')->with(['berhasil' => $message['message']]);
                    } else {
                        $message = ['message' => 'Gagal menambah pertandingan.'];
                        return redirect()->route('tambahmatch')->with(['error' => $message['message']]);
                    }
                }
            } catch (\Exception $e) {
                return redirect()->route('tambahmatch')->with(['error' => $e->getMessage()]);
            }
        }
    }

    public function stormatch(Request $request)
    {

        try {
            $idmatch = $request->idmatch;

            if ($idmatch === null) {
                $message = ['message' => 'Tidak ada skor yang diubah.'];
                return redirect()->route('listmatch')->with(['error' => $message['message']]);
            }

            // dd($idmatch);

            foreach ($idmatch as $key => $noid) {
                $data = DataPertandingan::find($noid);

                $result = $data->update(
                    [
                        'skor1' => $request->home[$key],
                        'skor2' => $request->away[$key],
                        'stat' => 1,
                    ]
                );

                if ($request->home[$key] === $request->away[$key]) {
                    $menang = 0;
                    $seri = 1;
                    $kalah = 0;
                } elseif ($request->home[$key] >= $request->away[$key]) {
                    $menang = 1;
                    $seri = 0;
                    $kalah = 0;
                } else {
                    $menang = 0;
                    $seri = 0;
                    $kalah = 1;
                }



                $datadetail = DetailClub::where('club_id', $data->klub1id);
                $cd1 = $datadetail->first();
                $datadetail2 = DetailClub::where('club_id', $data->klub2id);
                $cd2 = $datadetail2->first();

                $datadetail->update([
                    'menang' => $cd1->menang + $menang,
                    'seri' => $cd1->seri + $seri,
                    'kalah' => $cd1->kalah + $kalah,
                    'gol_masuk' => $cd1->gol_masuk + $data->skor1,
                    'gol_kemasukan' => $cd1->gol_kemasukan + $data->skor2,
                    'stat' => 1
                ]);

                if ($request->home[$key] === $request->away[$key]) {
                    $menang = 0;
                    $seri = 1;
                    $kalah = 0;
                } elseif ($request->home[$key] <= $request->away[$key]) {
                    $menang = 1;
                    $seri = 0;
                    $kalah = 0;
                } else {
                    $menang = 0;
                    $seri = 0;
                    $kalah = 1;
                }
                $datadetail2->update([
                    'menang' => $cd2->menang + $menang,
                    'seri' => $cd2->seri + $seri,
                    'kalah' => $cd2->kalah + $kalah,
                    'gol_masuk' => $cd2->gol_masuk + $data->skor2,
                    'gol_kemasukan' => $cd2->gol_kemasukan + $data->skor1,
                    'stat' => 1
                ]);
            }

            if ($result) {

                $message = ['message' => 'Skor pertandingan berhasil diubah.'];
                return redirect()->route('listmatch')->with(['berhasil' => $message['message']]);
            } else {
                $message = ['message' => 'Gagal mengubah skor pertandingan.'];
                return redirect()->route('listmatch')->with(['error' => $message['message']]);
            }
        } catch (\Exception $e) {
            return redirect()->route('listmatch')->with(['error' => $e->getMessage()]);
        }
    }


    public function storesatuan(Request $request, $id)
    {
        $data = DataPertandingan::find($id);

        return view('skor-satuan', ['match' => $data]);
    }
}