<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailClub extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function dataclub()
    {
        return $this->belongsTo(DataClub::class);
    }
}