<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataClub extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function detail()
    {
        return $this->hasMany(DetailClub::class, 'club_id');
    }

    public function dataklub1()
    {
        return $this->hasMany(DataPertandingan::class, 'klub1id');
    }

    public function dataklub2()
    {
        return $this->hasMany(DataPertandingan::class, 'klub2id');
    }
}