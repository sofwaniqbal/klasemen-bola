<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPertandingan extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function dataklub1()
    {
        return $this->belongsTo(DataClub::class);
    }

    public function dataklub2()
    {
        return $this->belongsTo(DataClub::class);
    }
}