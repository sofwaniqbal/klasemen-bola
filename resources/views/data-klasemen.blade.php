@extends('layout.layout')
@section('judul', 'Standing')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-md-8 offset-md-2 justify-content-center">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Klub</th>
                                    <th class="text-center">Main</th>
                                    <th class="text-center">Menang</th>
                                    <th class="text-center">Seri</th>
                                    <th class="text-center">Kalah</th>
                                    <th class="text-center">Gol Masuk</th>
                                    <th class="text-center">Gol Kemasukan</th>
                                    <th class="text-center">Selisih</th>
                                    <th class="text-center">Poin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataclub as $v)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $v->nama_klub }}</td>
                                        {{-- <td class="text-center">{{ $v->menang + $v->seri + $v->kalah }}</td> --}}
                                        <td class="text-center">{{ $v->main }}</td>
                                        <td class="text-center">{{ $v->menang }}</td>
                                        <td class="text-center">{{ $v->seri }}</td>
                                        <td class="text-center">{{ $v->kalah }}</td>
                                        <td class="text-center">{{ $v->gol_masuk }}</td>
                                        <td class="text-center">{{ $v->gol_kemasukan }}</td>
                                        <td class="text-center">{{ $v->selisih }}</td>
                                        <td class="text-center">{{ $v->poin }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
