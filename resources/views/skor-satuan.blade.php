@extends('layout.layout')
@section('judul', 'Match')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <a href="/match" style="text-decoration: none"><i class="fas fa-arrow-left"></i> Kembali</a>
            <div class="col-md-6 offset-md-3 justify-content-center">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div><br />
                @endif

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if ($message = Session::get('berhasil'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header text-center">
                        <div class="col-12 text-lg-center">
                            <b>Edit Skor</b>

                        </div>
                    </div>
                    <div class="card-body text-center align-self-center">

                        <form class="row g-3 text-center" method="POST" action="{{ url('simpan-skor') }}">
                            {{-- @foreach ($datamatch as $match) --}}
                            @csrf
                            <input type="hidden" name="idmatch[]" value="{{ $match->id }}"
                                {{ $match->stat === 0 ? '' : 'disabled' }}>
                            <div class="col-md-4 position-relative text-center">
                                <label for="validationTooltip01" class="form-label"><strong
                                        class="text-primary">Home</strong></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">{{ $match->home }}</span>
                                    <input type="hidden" name="klubhome[]" value="{{ $match->home }}"
                                        {{ $match->stat === 0 ? '' : 'disabled' }}>
                                    <input type="text" class="form-control" name="home[]"
                                        placeholder="{{ $match->home }}" aria-label="{{ $match->home }}"
                                        aria-describedby="basic-addon1" value="{{ $match->skor1 }}"
                                        {{ $match->stat === 0 ? '' : 'disabled' }}>
                                </div>
                            </div>

                            <div class="col-md-4 position-relative text-center">
                                <h3 class="text-primary">VS</h3>
                            </div>

                            <div class="col-md-4 position-relative text-center">
                                <label for="namaklub" class="form-label"><strong
                                        class="text-primary">Away</strong></label>
                                <div class="input-group mb-3">

                                    <input type="hidden" name="klubaway[]" value="{{ $match->away }}"
                                        {{ $match->stat === 0 ? '' : 'disabled' }}>
                                    <input type="text" class="form-control" name="away[]"
                                        placeholder="Skor {{ $match->away }}" aria-label="{{ $match->away }}"
                                        aria-describedby="basic-addon1" value="{{ $match->skor2 }}"
                                        {{ $match->stat === 0 ? '' : 'disabled' }}>
                                    <span class="input-group-text" id="basic-addon1">{{ $match->away }}</span>
                                </div>
                            </div>

                            {{-- @endforeach --}}
                            @if ($match !== 0)
                                <div class="col-12 text-lg-left">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            @endif
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
