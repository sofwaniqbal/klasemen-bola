@extends('layout.layout')
@section('judul', 'Tambah Match')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <a href="/match" style="text-decoration: none"><i class="fas fa-arrow-left"></i> Kembali</a>
            <div class="col-md-6 offset-md-3 justify-content-center">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if ($message = Session::get('berhasil'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header text-center">
                        <strong>Tambah Data Klub</strong>
                    </div>
                    <div class="card-body">
                        <form class="row g-3 text-center" method="POST" action="{{ url('simpan-match') }}">
                            @csrf
                            <div class="col-md-4 position-relative text-center">
                                <label for="validationTooltip01" class="form-label"><strong>Home</strong></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                                    <select class="form-control" name="homeklub">
                                        <option disabled>Select Product</option>
                                        @foreach ($dataklub as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 position-relative text-center">
                                <h3>VS</h3>
                            </div>
                            <div class="col-md-4 position-relative text-center">
                                <label for="namaklub" class="form-label"><strong>Away</strong></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                                    <select class="form-control" name="awayklub">
                                        <option disabled>Select Product</option>
                                        @foreach ($dataklub as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 text-lg-left">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                            </div>

                        </form>
                    </div>
                    <div class="card-footer text-muted">
                        <div class="col-12 text-lg-end">
                            <a href="/list-klub" type="button" class="btn btn-danger"><i class="fas fa-ban"></i>
                                Batal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
