@extends('layout.layout')
@section('judul', 'Tambah Klub')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <a href="/list-klub" style="text-decoration: none"><i class="fas fa-arrow-left"></i> Kembali</a>
            <div class="col-md-6 offset-md-3 justify-content-center">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        <p class="mb-0"><small>{{ $message }}</small></p>
                    </div>
                @endif

                @if ($message = Session::get('berhasil'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close text-right" data-bs-dismiss="alert"
                            aria-label="Close"></button>
                        <p class="mb-0"><small>{{ $message }}</small></p>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header text-center">
                        <strong>Tambah Data Klub</strong>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('simpan-klub') }}">
                            @csrf
                            <div class="row mb-3">
                                <label for="namaklub" class="col-sm-2 col-form-label">Nama Klub</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="namaklub" name="namaklub">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="kotaklub" class="col-sm-2 col-form-label">Kota Klub</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="kotaklub" name="kotaklub">
                                </div>
                            </div>

                            <div class="col-12 text-lg-left">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                            </div>

                        </form>
                    </div>
                    <div class="card-footer text-muted">
                        <div class="col-12 text-lg-end">
                            <a href="/list-klub" type="button" class="btn btn-danger"><i class="fas fa-ban"></i>
                                Batal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
