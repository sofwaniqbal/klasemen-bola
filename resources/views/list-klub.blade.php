@extends('layout.layout')
@section('judul', 'List Klub')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-md-6 offset-md-3 justify-content-center">
                <div class="card">
                    <div class="card-header text-lg-right">
                        <div class="col-12 text-lg-end">
                            <a href="/tambah-klub" type="button" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>
                                Tambah
                                Klub</a>
                        </div>

                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left">Klub</th>
                                    <th class="text-center">Kota</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dataklub as $klub)
                                    <tr>
                                        <td>{{ $klub->nama_klub }}</td>
                                        <td class="text-center">{{ $klub->kota }}</td>
                                        {{-- <td class="text-center">W</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
