<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">K<strong>B</strong></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == '' ? 'active' : '' }}" aria-current="page"
                        href="/">Standing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == 'match' ? 'active' : '' }}" href="/match">Match</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == 'list-klub' ? 'active' : '' }}"
                        href="/list-klub">List
                        Klub</a>
                </li>
            </ul>

        </div>
    </div>
</nav>
