@extends('layout.layout')
@section('judul', 'Match')

@section('css-after')
@stop

@section('main-content')
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-md-6 offset-md-3 justify-content-center">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div><br />
                @endif

                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if ($message = Session::get('berhasil'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="mb-0"><small>{{ $message }}</small></p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6 text-center">
                                    <a href="/skor-massal" type="button" class="btn btn-warning btn-sm"><i
                                            class="fas fa-pencil-alt"></i>
                                        Edit Skor Massal</a>
                                </div>
                                <div class="col-6 text-center">
                                    <a href="/tambah-pertandingan" type="button" class="btn btn-primary btn-sm"><i
                                            class="fas fa-plus"></i>
                                        Tambah
                                        Pertandingan</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center align-self-center">

                        <form class="row g-8 text-center" method="POST" action="{{ url('simpan-skor') }}">
                            @foreach ($datamatch as $match)
                                @csrf
                                <hr>
                                <input type="hidden" name="idmatch[]" value="{{ $match->id }}"
                                    {{ $match->stat === 0 ? '' : 'disabled' }}>

                                <div class="col-md-4  text-center">
                                    <label for="validationTooltip01" class="form-label"><strong
                                            class="text-primary">Home</strong></label>
                                    <div class="input-group mb-3 justify-content-center">
                                        <input type="hidden" name="klubhome[]" value="{{ $match->home }}"
                                            {{ $match->stat === 0 ? '' : 'disabled' }}>
                                        <h2>{{ $match->stat === 0 ? '-' : $match->skor1 }}</h2>
                                        {{-- <input type="text" class="form-control" name="home[]"
                                            placeholder="{{ $match->home }}" aria-label="{{ $match->home }}"
                                            aria-describedby="basic-addon1" value="{{ $match->skor1 }}"
                                            {{ $match->stat === 0 ? '' : 'disabled' }}> --}}
                                    </div>
                                    <label for="validationTooltip01" class="form-label"><strong class="text-primary">
                                            <h3>{{ $match->home }}</h3>
                                        </strong></label>
                                </div>

                                <div class="col-md-4  text-center">
                                    <h3 class="text-primary">VS</h3>
                                    @if ($match->stat === 0)
                                        <a href="/skor-satuan/{{ $match->id }}" type="button"
                                            class="btn btn btn-warning btn-sm"><i class="fas fa-pencil-alt"></i>
                                            Skor</a>
                                    @else
                                        <p><strong><small class=" text-monospace"><u>FT</u></small></strong></p>

                                    @endif
                                </div>

                                <div class="col-md-4  text-center">
                                    <label for="namaklub" class="form-label"><strong
                                            class="text-primary">Away</strong></label>
                                    <div class="input-group mb-3 justify-content-center">

                                        <input type="hidden" name="klubaway[]" value="{{ $match->away }}"
                                            {{ $match->stat === 0 ? '' : 'disabled' }}>
                                        <h2>{{ $match->stat === 0 ? '-' : $match->skor2 }}</h2>
                                        {{-- <input type="text" class="form-control" name="away[]"
                                            placeholder="Skor {{ $match->away }}" aria-label="{{ $match->away }}"
                                            aria-describedby="basic-addon1" value=""
                                            {{ $match->stat === 0 ? '' : 'disabled' }}> --}}

                                    </div>
                                    <label for="validationTooltip01" class="form-label"><strong class="text-primary">
                                            <h3>{{ $match->away }}</h3>
                                        </strong></label>
                                </div>

                            @endforeach
                            {{-- @if ($datamatch !== [])
                                <div class="col-12 text-lg-left">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            @endif --}}
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-after')
@stop
