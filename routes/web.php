<?php

use App\Http\Controllers\DataClubController;
use App\Http\Controllers\DataPertandinganController;
use App\Http\Controllers\DetailClubController;
use App\Models\DataClub;
use App\Models\DataPertandingan;
use App\Models\DetailClub;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {

// });

Route::get('/', [DetailClubController::class, 'index']);

Route::get('/match', function () {
    $datamatch = DataPertandingan::select('id', 'home', 'away', 'skor1', 'skor2', 'stat')
        ->get();

    if (count($datamatch) === 0) {
        return view('pertandingan', ['datamatch' => []]);
    } else {
        return view('pertandingan', ['datamatch' => $datamatch]);
    }
})->name('listmatch');

Route::get('/list-klub', function () {
    $dataclub = DataClub::all();
    return view('list-klub', ['dataklub' => $dataclub]);
});

Route::get('/tambah-klub', function () {
    return view('tambah-klub');
})->name('tambahklub');

Route::get('/skor-massal', function () {
    $datamatch = DataPertandingan::select('id', 'home', 'away', 'skor1', 'skor2', 'stat')
        ->where('stat', 0)->get();

    if (count($datamatch) === 0) {
        return view('skor-massal', ['datamatch' => []]);
    } else {
        return view('skor-massal', ['datamatch' => $datamatch]);
    }
})->name('skormassal');

Route::get('/tambah-pertandingan', function () {
    $dataclub = DataClub::pluck('nama_klub', 'id');
    return view('tambah-match', ['dataklub' => $dataclub]);
})->name('tambahmatch');

Route::post('/simpan-klub', [DataClubController::class, 'store']);
Route::get('/simpan-klub', [DataClubController::class, 'auth']);

Route::post('/simpan-match', [DataPertandinganController::class, 'store']);
Route::get('/simpan-match', [DataPertandinganController::class, 'auth']);

Route::post('/simpan-skor', [DataPertandinganController::class, 'stormatch']);
Route::get('/simpan-skor', [DataPertandinganController::class, 'auth']);

Route::get('/skor-satuan/{id}', [DataPertandinganController::class, 'storesatuan']);
// Route::get('/skor-satuan', [DataPertandinganController::class, 'auth']);
